package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// Rule holds the schema definition for the Rule entity.
type Rule struct {
	ent.Schema
}

// Fields of the Rule.
func (Rule) Fields() []ent.Field {
	return []ent.Field{
		field.Bool("disabled").
			Default(true),
		field.String("categorie").
			Default("Malediction"),
		field.String("title").
			Default("unknow"),
		field.String("instruction").
			Default("unknown"),
		field.Int("ratio").
			Default(0),
		field.Int("like").
			Default(0),
		field.Int("dislike").
			Default(0),
		field.UUID("uuid", uuid.UUID{}).
			Default(uuid.New),
	}
}

// Edges of the Rule.
func (Rule) Edges() []ent.Edge {
	return nil
}
