package server

import (
	"log"
	"net/http"
)

func router() {
	http.HandleFunc("/", homePage)
	http.HandleFunc("/ModifyRule", modifyRule)

	log.Fatal(http.ListenAndServe(":10000", nil))
}
