package main

import (
	"bob/ent"
	"bob/ent/rule"
	"context"
	"fmt"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

func CreateRule(ctx context.Context, client *ent.Client) (*ent.Rule, error) {
	u, err := client.Rule.
		Create().
		SetCategorie("Malediction").
		SetInstruction("Tu deviens le jumeau malefique de victor on t'apelle desormais Wavictor").
		Save(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed creating Rule: %w", err)
	}
	log.Println("Rule was created: ", u)
	return u, nil
}

func QueryRule(ctx context.Context, client *ent.Client) (*ent.Rule, error) {
	u, err := client.Rule.
		Query().
		Where(rule.Instruction("Test")).
		// `Only` fails if no Rule found,
		// or more than 1 Rule returned.
		Only(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed querying Rule: %w", err)
	}
	log.Println("Rule returned: ", u)
	return u, nil
}

var (
	WarningLogger *log.Logger
	InfoLogger    *log.Logger
	ErrorLogger   *log.Logger
)

// init function in golang
func init() {
	fmt.Println("Init go")

	file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	InfoLogger = log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	WarningLogger = log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	ErrorLogger = log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}

func main() {
	fmt.Println("Hello Go")

	InfoLogger.Println("Starting the application...")
	InfoLogger.Println("Something noteworthy happened")
	WarningLogger.Println("There is something you should know about")
	ErrorLogger.Println("Something went wrong")
	// If the file doesn't exist, create it or append to the file
	file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	log.SetOutput(file)

	client, err := ent.Open("sqlite3", "bob.db?mode=memory&cache=shared&_fk=1")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}
	defer client.Close()

	ctx := context.Background()
	// Run the auto migration tool.
	if err := client.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	if _, err := CreateRule(ctx, client); err != nil {
		log.Fatalf("Chat bite au createRule", err)
	}

}
