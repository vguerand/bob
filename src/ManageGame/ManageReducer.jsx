import { useReducer } from "react";
import Reducers from "./Reducers";

/**
 * Manage all reducer inside a root reducer
 */
const childReducers = new Map();

childReducers.set(Reducers.GAME_REDUCER, Reducers.gameReducer);
childReducers.set(Reducers.BASE_REDUCER, Reducers.baseReducer);

const rootReducer = (state, action) => {
  console.log("[rootREDUCER] BEFORE AN ACTION", action, state);
  const reducer = childReducers.get(action.reducer);
  state = reducer(state, action);
  console.log("[rootREDUCER] AFTER AN ACTION", action, state);
  return state;
};

const useManageReducer = (initialState) => {
  console.log("[useManageReducer]", initialState);
  const [state, dispatch] = useReducer(rootReducer, initialState);
  return [state, dispatch];
};

export default useManageReducer;
