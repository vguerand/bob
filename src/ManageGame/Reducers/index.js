import gameReducer from "./Game/GameReducer";
import baseReducer from "./Base/BaseReducer";
import { GAME_REDUCER } from "./Game/GameActions";
import { BASE_REDUCER } from "./Base/BaseActions";

export default {
  // reducer
  gameReducer,
  baseReducer,

  // type of reducer
  GAME_REDUCER,
  BASE_REDUCER,
};
