import { getRandomInt } from "../../../helpers/functions";
import { TypeOfRuleConstant } from "../../../Views/Rule/TypeOfRuleConstant";
import findRule from "./findRule";
import {
  ADD_PLAYER,
  INITIALIAZE_RULE,
  NEXT_RULE,
  SAVE_RULES,
  DELETE_PLAYER
} from "./GameActions";

/**
 * This is Reducer for Game Management
 *
 * @param {state}  : Game state
 * @param {action} : Evenement
 */
const gameReducer = (state, action) => {
  switch (action.type) {
    case NEXT_RULE:
      return {
        ...state,
        currentPlayer:
          state.listPlayers[getRandomInt(0, state.listPlayers.length)],
        displayRule: findRule(state.listRules),
      };
    case ADD_PLAYER:
      let tmpListPlayer = [...state.listPlayers, action.playerName]
      localStorage.setItem("players", JSON.stringify(tmpListPlayer))
      return {
        ...state,
        listPlayers: tmpListPlayer,
      };
    case DELETE_PLAYER:
      // Delete the player Name inside the list
      let filterPlayers = state.listPlayers.filter(item => item !== action.playerName)
      localStorage.setItem("players", JSON.stringify(filterPlayers))
      return {
        ...state,
        listPlayers: filterPlayers,
      };
    case SAVE_RULES:
      localStorage.setItem("rules", JSON.stringify(state.listRules));
      return {
        ...state,
      };
    case INITIALIAZE_RULE:
      const tmpRules = [
        ...TypeOfRuleConstant.Bénediction,
        ...TypeOfRuleConstant.Sentences,
        ...TypeOfRuleConstant.Rôle,
      ];
      return {
        ...state,
        listRules: tmpRules,
      };
    default:
      return state;
  }
};

export default gameReducer;
