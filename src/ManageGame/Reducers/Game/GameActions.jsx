export const GAME_REDUCER = "GAME_REDUCER";
export const ADD_PLAYER = "ADD_PLAYER";
export const NEXT_RULE = "NEXT_RULE";
export const SAVE_RULES = "SAVE_RULES";
export const DELETE_PLAYER = "DELETE_PLAYER";


export const INITIALIAZE_RULE = "INITIALIAZE_RULE";

/**
 * export const actionAddNewPlayer = (player) => {
 */
export const actionAddNewPlayer = (player) => {
  return {
    playerName: player,
    type: ADD_PLAYER,
    reducer: GAME_REDUCER,
  };
};

/**
 * export const actionDeletePlayer = (player) => {
 */
 export const actionDeletePlayer = (player) => {
  return {
    playerName: player,
    type: DELETE_PLAYER,
    reducer: GAME_REDUCER,
  };
};



/**
 * export const actionNextRule = (rule) => {
 */
export const actionNextRule = (rule) => {
  return {
    rule: rule,
    type: NEXT_RULE,
    reducer: GAME_REDUCER,
  };
};

/**
 * actionSaveRules
 *
 */
export const actionSaveRules = () => {
  return {
    type: SAVE_RULES,
    reducer: GAME_REDUCER,
  };
};

/**
 * actionInitializeRule
 *
 */
export const actionInitializeRule = () => {
  return {
    type: INITIALIAZE_RULE,
    reducer: GAME_REDUCER,
  };
};
