export const BASE_REDUCER = "BASE_REDUCER";

export const CLOSE_MODAL = "CLOSE_MODAL";
export const OPEN_MODAL = "OPEN_MODAL";

/**
 * actionOpenModal
 *
 */
export const actionOpenModal = (msgModal, typeModal) => {
  return {
    msgModal: msgModal,
    modal: typeModal,
    type: OPEN_MODAL,
    reducer: BASE_REDUCER,
  };
};

/**
 * actionCloseModal
 *
 */
export const actionCloseModal = () => {
  return {
    type: CLOSE_MODAL,
    reducer: BASE_REDUCER,
  };
};
