import { CLOSE_MODAL, OPEN_MODAL } from "./BaseActions";

/**
 * This is Reducer for teacher state
 *
 * @param {state}  : teacher state
 * @param {action} : Evenement
 */
const baseReducer = (state, action) => {
  console.log("baseReducer", state);
  console.log("Reducer Teacher", action, state);
  switch (action.type) {
    case CLOSE_MODAL:
      /**
       *  The variable modale is set with a constant
       *  ModalConstant.jsx
       */
      return { ...state, ...{ modal: action.modal } };
    case OPEN_MODAL:
      return {
        ...state,
        ...{ modal: action.modal, messageModal: action.msgModal },
      };

    default:
      return state;
  }
};

export default baseReducer;
