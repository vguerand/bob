import React, { createContext, useState, useEffect } from "react";
import PropTypes from "prop-types";
import useManageReducer from "./ManageReducer";
import ManageModal from "../Modal/ManageModal";
import ModalConstant from "../Modal/ModalConstant";
import BurgerMenu from "../Components/BugerMenu";
import { actionOpenModal } from "./Reducers/Base/BaseActions";
import { TypeOfRuleConstant } from "../Views/Rule/TypeOfRuleConstant";
import { actionInitializeRule } from "./Reducers/Game/GameActions";

const initialContext = {
  state: {},
  dispatch: () => {},
};

const initialStateGame = {
  listPlayers: [],
  currentPlayer: "Bob",
  modal: ModalConstant.NoModal,
  listRules:  TypeOfRuleConstant.Bénediction,
  displayRule: null,
  typeOfRule: "Bénédiction",
};

export const GameContext = createContext(initialContext);

const GameContextProvider = ({ children }) => {
  const [state, dispatch] = useManageReducer(initialStateGame);

  useEffect(() => {
    console.log("[GameContext] mount");

    let players = localStorage.getItem("players")
    if (players) {
      state.listPlayers = JSON.parse(players)
    }
    let rules = localStorage.getItem("rules")
    if (rules) {
      state.listRules = JSON.parse(rules)
    }
    dispatch(actionInitializeRule());
    return () => {};
  }, [""]);

  console.log("[GameContextReducer", state);
  return (
    <GameContext.Provider
      value={{
        state,
        dispatch,
      }}
    >
      <ManageModal />
      {children}
      <BurgerMenu
        openMenu={() =>
          dispatch(
            actionOpenModal("BURGER MENU", ModalConstant.ModalBurgerMenu)
          )
        }
      />
    </GameContext.Provider>
  );
};

GameContextProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

GameContextProvider.defaultProps = {};

export default GameContextProvider;
