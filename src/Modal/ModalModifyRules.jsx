import React from "react";
import { PropTypes } from "prop-types";

import BasicModal from "../Components/BasicModal";
import { actionCloseModal } from "../ManageGame/Reducers/Base/BaseActions";

const ModalModifyRules = ({ dispatch }) => {
  return (
    <BasicModal toggle onToggle={() => dispatch(actionCloseModal())}>
      <div className="col-flex-centered" style={{ height: `100%` }}>
        <div className="row-flex-centered">Modifer les régles du Jeu</div>
      </div>
    </BasicModal>
  );
};

ModalModifyRules.propTypes = {
  dispatch: PropTypes.bool,
};

ModalModifyRules.defaultProps = {
  dispatch: (el) => console.error("Try to dispatch ", el),
};

export default ModalModifyRules;
