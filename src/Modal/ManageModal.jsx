import React, { useState, useEffect, useContext } from "react";
import { useHistory } from "react-router-dom";

/**
 * Modals imports
 */
import ModalModifyPlayer from "./ModalModifyPlayer";
import ModalModifyRules from "./ModalModifyRules";
import { GameContext } from "../ManageGame/GameContext";
import ModalConstant from "./ModalConstant";
import ModalBurgerMenu from "./ModalBurgerMenu";

/**
 * Ce fichier liste les differentes modales appellées depuis le
 * student layout.
 */

const ManageModal = () => {
  //   const history = useHistory();
  const { dispatch, state } = useContext(GameContext);

  const modalStep = [
    null,
    <ModalModifyPlayer dispatch={dispatch} />,
    <ModalModifyRules dispatch={dispatch} />,
    <ModalBurgerMenu dispatch={dispatch} />,
  ];
  if (state.modal === ModalConstant.NoModal) {
    return <> </>;
  }
  return <>{modalStep[state.modal]}</>;
};

export default ManageModal;
