import React from "react";
import { PropTypes } from "prop-types";

import BasicModal from "../Components/BasicModal";
import { actionCloseModal } from "../ManageGame/Reducers/Base/BaseActions";
import { useHistory } from "react-router";

const ModalBurgerMenu = ({ dispatch }) => {
  const history = useHistory();
  return (
    <BasicModal toggle onToggle={() => dispatch(actionCloseModal())}>
      <div className="menu">
        <button
          className="no-button"
          color="danger"
          onClick={() => history.push("/") + dispatch(actionCloseModal())}
        >
          Page d'acceuil
        </button>
        <hr />
        <button
          className="no-button"
          color="danger"
          onClick={() => history.push("/regle") + dispatch(actionCloseModal())}
        >
          Voir les règles
        </button>
        <hr />
      </div>
    </BasicModal>
  );
};

ModalBurgerMenu.propTypes = {
  dispatch: PropTypes.bool,
};

ModalBurgerMenu.defaultProps = {
  dispatch: (el) => console.error("Try to dispatch ", el),
};

export default ModalBurgerMenu;
