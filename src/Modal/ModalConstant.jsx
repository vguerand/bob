/**
 *  Status Constant Enum
 *
 *  List of the different modal
 * @readonly
 * @enum {number}
 */

const ModalConstant = {
  NoModal: 0,
  ModalModifyPlayer: 1,
  ModalModifyRules: 2,
  ModalBurgerMenu: 3,
};

export default ModalConstant;
