import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import GamePage from "./Views/Game/GamePage";
import AdminPage from "./Views/Rule/RulePage";
import AddPlayerPage from "./Views/Home/AddPlayerPage";
import GameContextProvider from "./ManageGame/GameContext";
import BasicRules from "./Views/Rule/BasicRules";

ReactDOM.render(
  <BrowserRouter>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossOrigin="anonymous"
    />
    <Switch>
      <Route
        path="/"
        exact
        render={() => (
          <GameContextProvider>
            <AddPlayerPage />
          </GameContextProvider>
        )}
      />
      <Route
        path="/game"
        render={() => (
          <GameContextProvider>
            <GamePage />
          </GameContextProvider>
        )}
      />

      <Route
        exact
        path="/regle"
        render={() => (
          <GameContextProvider>
            <AdminPage />
          </GameContextProvider>
        )}
      />
      <Route
        path="/regle/:typeOfRule"
        render={() => (
          <GameContextProvider>
            <BasicRules />
          </GameContextProvider>
        )}
      />
      <Redirect to="/" />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);
