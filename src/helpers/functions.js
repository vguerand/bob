export const getRandomInt = (min, max) => {
  return Math.floor(min + Math.random() * Math.floor(max - min));
};

export const isJson = (str) => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};
