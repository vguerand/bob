import React, { useContext, useState } from "react";
import { useHistory, useLocation, useParams } from "react-router";
import { GameContext } from "../../ManageGame/GameContext";
import { actionOpenModal } from "../../ManageGame/Reducers/Base/BaseActions";
import ModalConstant from "../../Modal/ModalConstant";
import { TypeOfRuleConstant, KeysOfRuleConstant } from "./TypeOfRuleConstant";

const BasicRules = () => {
  const { state, dispatch } = useContext(GameContext);
  const history = useHistory();
  const { typeOfRule } = useParams();

  console.log("BasicRules", typeOfRule);
  if (typeOfRule === "all") {
    return (
      <div className="page">
        {state.listRules.map((el) => (
          <div
            className="rule is-size-3"
            onClick={(el) => {
              console.log("Modale");
              dispatch(actionOpenModal(el, ModalConstant.ModalModifyRules));
            }}
          >
            {el.instruction}
            <button type="checkbox">X</button>
            {/* <div className="button-rule">
                <button>O</button>
                <button>O</button>
              </div> */}
          </div>
        ))}
      </div>
    );
  }
  const Rules = TypeOfRuleConstant[typeOfRule].map((el) => (
    <div
      className="rule is-size-3"
      onClick={(el) => {
        console.log("Modale");
        dispatch(actionOpenModal(el, ModalConstant.ModalModifyRules));
      }}
    >
      {el.instruction}
      <button type="checkbox">X</button>
      {/* <div className="button-rule">
              <button>O</button>
              <button>O</button>
            </div> */}
    </div>
  ));
  return (
    <div className="page">
      <h1> {typeOfRule} </h1>
      {Rules}
    </div>
  );
};

export default BasicRules;
