import sentences from "../../GameRules/sentences.json";
import benediction from "../../GameRules/benedictions.json";
import questions from "../../GameRules/questions.json";
import role from "../../GameRules/role.json";
import jeux from "../../GameRules/jeux.json";

/**
 *  Status Constant Enum
 *
 *  List of the different type of rule
 * @readonly
 * @enum {number}
 */

const TypeOfRuleConstant = {
  Sentences: sentences,
  Bénediction: benediction,
  Rôle: role,
  //   Jeux: jeux,
  //   "Malediction",
  //   ALL: questions,
};

const KeysOfRuleConstant = ["Sentences", "Bénediction", "Rôle"];

export { TypeOfRuleConstant, KeysOfRuleConstant };
