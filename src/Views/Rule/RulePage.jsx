import React, { useContext, useState } from "react";
import { useHistory } from "react-router";
import { Button, Col, Row } from "reactstrap";
import Questions from "../../GameRules/questions.json";
import { GameContext } from "../../ManageGame/GameContext";
import { actionOpenModal } from "../../ManageGame/Reducers/Base/BaseActions";
import ModalConstant from "../../Modal/ModalConstant";
import { TypeOfRuleConstant, KeysOfRuleConstant } from "./TypeOfRuleConstant";

const ChoiceTypeOfRule = ({ setTypeRule }) => {
  const history = useHistory();
  return (
    <div className="menu-type-of-rule">
      {KeysOfRuleConstant.map((el, idx) => {
        return (
          <h1
            className="title-type-of-rule"
            onClick={() => setTypeRule(el) + history.push("/regle/" + el)}
          >
            {el}
          </h1>
        );
      })}
      <h1
        className="title-type-of-rule"
        onClick={() => history.push("/regle/all")}
      >
        Toutes les régles
      </h1>
    </div>
  );
};

const RulePage = () => {
  const [typeOfRule, setTypeRule] = useState(KeysOfRuleConstant[0]);
  console.log("Key of rules constants", TypeOfRuleConstant, typeOfRule);
  const { state, dispatch } = useContext(GameContext);

  return (
    <div className="">
      <ChoiceTypeOfRule setTypeRule={setTypeRule} />
    </div>
  );
};

export default RulePage;
