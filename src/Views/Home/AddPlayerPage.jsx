import React, { useContext, useState } from "react";
import {
  Button,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
import { GameContext } from "../../ManageGame/GameContext";
import { actionAddNewPlayer, actionDeletePlayer } from "../../ManageGame/Reducers/Game/GameActions";
import { useHistory } from "react-router";

const AddPlayerPage = () => {
  const { state, dispatch } = useContext(GameContext);
  const history = useHistory();
  const { listPlayers } = state;
  const [newPlayer, setNewPlayer] = useState("");

  console.log("[AddPlayerPage]", state);

  const sendNewPlayer = () => {
    if (newPlayer) { // && newPlayer.length > 3) {
      console.log("[OnClick sendNewPlayer]", newPlayer);
      dispatch(actionAddNewPlayer(newPlayer));
      setNewPlayer("");
    }
  };

  // Update Player

  const deletePlayer = (playerName) => {
    dispatch(actionDeletePlayer(playerName))
  }

  return (
    <div className="page">
      <div className="add-player-page">
        <h1 className="game-text"> BoB </h1>
        <div class="input-group">
          <input type="text" name="search" class="form-control" placeholder="Ajoute un joueur"
            onChange={(el) => setNewPlayer(el.currentTarget.value)}
          />
          <span>
            <button type="submit" class="btn btn-primary button-add-player"
              onClick={() => sendNewPlayer()}
            >
              +
            </button>
          </span>
        </div>
        <hr />
        {listPlayers.map((el, i) => (
          <div key={"key" + el}>
            <div class="input-group" >
              <input
                disabled
                style={{ border: "none", textAlign: "center" }}
                type="text"
                key="input_title"
                value={listPlayers[i] || ""}
                class="form-control" 
              // disabled={mode >= 2}
              />
              <span>
                <button type="submit" class="btn btn-danger button-add-player"
                  onClick={() => deletePlayer(el)}
                >
                  X
                </button>
              </span>
            </div>
            <hr/>
          </div>
        ))}
        <button className="btn btn-primary" onClick={() => history.push("/game")}>
          PLAY
        </button>
      </div>
    </div>
  );
};

export default AddPlayerPage;
