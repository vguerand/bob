import React, { useContext, useState } from "react";
import { Button } from "reactstrap";
import Questions from "../../GameRules/questions.json";
import { GameContext } from "../../ManageGame/GameContext";
import { actionNextRule } from "../../ManageGame/Reducers/Game/GameActions";

const GamePage = () => {
  const [step, setStep] = useState(0);
  const { state, dispatch } = useContext(GameContext);
  const questions = Questions.sentences;

  console.log("[GamePage] displayRule", state.displayRule);
  const nextStep = () => {
    if (step + 1 < questions.length) {
      setStep(step + 1);
      dispatch(actionNextRule());
    }
  };

  const replaceByName = () => {};

  return (
    <div className="page" onClick={() => nextStep()}>
      <div className={"game-background game-" + state.typeOfRule}>
        <div className={"game-text"}>
          {state.displayRule && state.displayRule.instruction ? (
            state.displayRule.instruction.replace("(?)", state.currentPlayer)
          ) : (
            <></>
          )}
          {/* <Button color="danger" onClick={() => nextStep()}>
            Suivant
          </Button> */}
        </div>
      </div>
    </div>
  );
};

export default GamePage;
