import React from "react";
import { PropTypes } from "prop-types";
import { Button } from "reactstrap";
import { actionCloseModal } from "../ManageGame/Reducers/Base/BaseActions";
// import "./components.css";
/*
 * A basic centered modal with veil size 500*600px
 */
const BasicModal = ({ toggle, children, footer, onToggle }) => {
  return (
    <>
      <div className="basic-modal-veil" />
      <div className="basic-modal basic-card flat-shadow">
        <div className="basic-modal-body p-3">
          {toggle ? (
            <Button
              className="basic-modal-header-toggle"
              onClick={() => onToggle()}
            >
              X
            </Button>
          ) : null}
          {children}
          <div className="basic-modal-footer">{footer}</div>
        </div>
      </div>
    </>
  );
};

BasicModal.propTypes = {
  toggle: PropTypes.bool,
  dispatch: PropTypes.func,
  children: PropTypes.node,
  footer: PropTypes.node,
  disabled: PropTypes.bool,
};

BasicModal.defaultProps = {
  toggle: false,
  onToggle: () => console.error("Try to Toggle"),
  children: "",
  footer: <div />,
  disabled: false,
};

export default BasicModal;
