import bob from "../assets/bob_logo.png";

const BurgerMenu = ({ activate, openMenu }) => {
  if (activate) return <></>;
  return (
    <div className="burger-menu-icon">
      <img
        src={bob}
        onClick={() => openMenu()}
        className={"burger-menu-image"}
      />
    </div>
  );
};

export default BurgerMenu;
