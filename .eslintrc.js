// module.exports = {
//   extends: ["airbnb", "plugin:prettier/recommended", "prettier/react"],
//   plugins: ["only-warn"],
//   settings: {
//     "import/resolver": {
//       node: {
//         paths: ["src"],
//       },
//     },
//   },
//   parser: "babel-eslint",
//   parserOptions: {
//     ecmaVersion: 2020,
//   },
//   env: {
//     browser: true,
//     commonjs: true,
//     es6: true,
//     jest: true,
//     node: true,
//   },
//   rules: {
//     "no-console": "off",
//     "jsx-a11y/href-no-hash": ["off"],
//     "react/jsx-filename-extension": [
//       "warn",
//       {
//         extensions: [".js", ".jsx", ".ts", ".tsx"],
//       },
//     ],
//     "max-len": [
//       "warn",
//       {
//         code: 100,
//         tabWidth: 2,
//         comments: 100,
//         ignoreComments: false,
//         ignoreTrailingComments: true,
//         ignoreUrls: true,
//         ignoreStrings: true,
//         ignoreTemplateLiterals: true,
//         ignoreRegExpLiterals: true,
//       },
//     ],
//     "import/extensions": [
//       "warn",
//       {
//         ts: "never",
//         tsx: "never",
//         js: "never",
//         jsx: "never",
//       },
//     ],
//     "max-classes-per-file": ["off"],
//     "lines-between-class-members": "off",
//     "no-param-reassign": ["warn", { props: false }],
//     "no-use-before-define": "off",
//     "@typescript-eslint/no-use-before-define": ["warn"],
//     "@typescript-eslint/no-explicit-any": "off",
//     "@typescript-eslint/no-inferrable-types": "off",
//     "@typescript-eslint/no-non-null-assertion": "off",
//   },
// };
